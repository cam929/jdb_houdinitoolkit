# README #

A repo of my free Houdini tools:

-ProSequencer: https://jdbgraphics.nl/script/prosequencer-2-0-houdini/

-ProBezierAssist https://jdbgraphics.nl/script/probezierassist-houdini/

-ProNavigator: https://jdbgraphics.nl/script/pronavigator-houdini/

-MatViewer:  https://vimeo.com/341562345



# INSTALL #
Clone the "JDB_HoudiniToolKit" repo into a folder of choice and add the following to your Houdini's env file, replace  '<path/to/your/folder/>'  with your own:


HOUDINI_PATH = "$HOUDINI_PATH;<path/to/your/folder>/;&"

HOUDINI_CUSTOM_PATH = "$HOUDINI_PATH;<path/to/your/folder>/python_panels;&"

So for example in my case you would end up with something like this:

HOUDINI_PATH = "$HOUDINI_PATH;E:/GIT/Houdini/;&"

HOUDINI_CUSTOM_PATH = "$HOUDINI_PATH;E:/GIT/Houdini/python_panels;&"

